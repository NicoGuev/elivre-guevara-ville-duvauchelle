package com.mycompany.mavenproject_elivre;


public class Livres extends Media{
    
    private int nb_pages;
    
    public Livres(String titre2, String auteur2, double prix2, int nb_pages2) {
        super(titre2,auteur2,prix2);
        this.setNb_pages(nb_pages2);
    }

    public String affichel() {
        if(!(this instanceof Bandes_Dessinées))
            return "> Livre\n" + this.affiche() + "\n\tNombre de pages : " + nb_pages;
        else
            return this.affiche() + "\n\tNombre de pages : " + nb_pages;
    }
    
   
    public int getNb_pages() {
        return nb_pages;
    }
    public void setNb_pages(int nb_pages) {
        this.nb_pages = nb_pages;
    }
            
}
