package com.mycompany.mavenproject_elivre;


public class Application
{
    public static void main( String[] args )
    {
        Livres HP, IM, AM, DN, ER;
        Bandes_Dessinées asterix, tintin, utopia, blake;
        DVD deadpool, interstellar, indianaj;
        Utilisateurs N, A, F;
        
        
        HP = new Livres("Harry Potter", "J.K. Rowling", 15.0, 500);
        IM = new Livres("Influence et Manipulation","Robert Cialdini",9,400);
        AM = new Livres("Animorphs : L'Invasion", "K. A. Applegate", 19.0, 250);
        DN = new Livres("Death Note Tome 1", "Tsugumi Oba", 10.0, 95);
        ER = new Livres("Eragon","Christopher Paolini",20.0, 300);
        asterix = new Bandes_Dessinées("Asterix et Obélix","Goscinny - Uderzo",12.0,35,"Couleur");
        tintin = new Bandes_Dessinées("Les aventures de Tintin / Tintin au Congo","Hergé",13.0,25,"Noir & Blanc");
        utopia = new Bandes_Dessinées("Utopia","Philip Carvel / Mark Dane",9999999,15,"Couleur");
        blake = new Bandes_Dessinées("Blake & Mortimer","Edgar P. Jacobs",12.0,30,"Couleur");
        deadpool = new DVD("DeadPool","Marvel",30,2.4);
        interstellar = new DVD("Interstellar","Christopher Nolan",29,2.3);
        indianaj = new DVD("Indiana Jones et la dernière Croisade","Steven Spielberg",10,2.1);
        
        
        N = new Utilisateurs("Guevara","Nicolas","Piggle","1234");
        N.setCagnotte(150);
        N.addMedia(HP,IM,deadpool); N.addMedia(asterix,tintin,utopia);
        System.out.println(N.affiche());
        
        A = new Utilisateurs("Duvauchelle","Alexis","xorbdu80","1202");
        A.setCagnotte(60);
        A.addMedia(AM,DN,asterix); A.addMedia(interstellar);
        System.out.println(A.affiche());
        
        F = new Utilisateurs("Ville","Fabien","faville","5496");
        F.setCagnotte(1000);
        F.addMedia(ER, blake, indianaj);
        System.out.println(F.affiche());
        
        F.echange(blake, N, utopia);
        System.out.println(F.affiche());
        
        N.vend(utopia, A);
        
        N.vend(tintin, F);
        System.out.println(F.affiche());
        System.out.println(N.affiche());
        
        N.echange(deadpool,A,interstellar);
        System.out.println(A.affiche());
        System.out.println(N.affiche());
        
        N.vend(interstellar, A);
        System.out.println(A.affiche());
        System.out.println(N.affiche());
    }
}
