package com.mycompany.mavenproject_elivre;


public class DVD extends Media {
    
    private double duree;
    
    public DVD(String titre2, String auteur2, double prix2, double duree2) {
        super(titre2,auteur2,prix2);
        this.setDuree(duree2);
    }

    public String affichedvd() {
        return "> Film\n" + this.affiche() + "\n\tDurée du film : " + (int)(duree - duree%1) +"h"+Math.round(duree%1*100);
    }
    
   
    public double getDuree() {
        return duree;
    }
    public void setDuree(double duree) {
        this.duree = duree;
    }
    
}
