package com.mycompany.mavenproject_elivre;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;


public class Utilisateurs {
    private String nom;
    private String prenom;
    private String login;
    private String mdp;
    private double cagnotte=0;
    private List<Media> biblio=new LinkedList<Media>();

    
    public Utilisateurs(String nom, String prenom, String login, String mdp) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setLogin(login);
        this.setMdp(mdp);
    }
 
    public void addMedia(Media l)
    {
        biblio.add(l);
    }
    public void addMedia(Media l1, Media l2)
    {
        biblio.add(l1); biblio.add(l2);
    }
    public void addMedia(Media l1, Media l2, Media l3)
    {
        biblio.add(l1); biblio.add(l2); biblio.add(l3);
    }

    
    public String affiche(){
        String tmp="";
        for(int i=0;i<biblio.size();i++)
        {
            if(biblio.get(i) instanceof Livres)
            {
                if(biblio.get(i) instanceof Bandes_Dessinées)
                    tmp=tmp+"\n"+((Bandes_Dessinées)(biblio.get(i))).affichebd();
                else
                    tmp=tmp+"\n"+((Livres)(biblio.get(i))).affichel();
            }else if(biblio.get(i) instanceof DVD)
                    tmp=tmp+"\n"+((DVD)(biblio.get(i))).affichedvd();
                else
                    tmp=tmp+"\n"+biblio.get(i).affiche();
        }
        return "\n[-----"+login+"-----]\nNom : "+nom+"\nPrenom : "+prenom+"\nCagnotte : "+cagnotte+"€\n---> Bibliothèque : "+tmp;
    }

    public void vend(Media l, Utilisateurs u)
    {
        //Test si il a bien le Media en question
        int test_appartient=-1;
        for(int i=0;i<biblio.size();i++)
        {
            if(l==biblio.get(i))
                test_appartient=i;
        }
        
        //vente
        if(test_appartient!=-1 && l.getPrix()<u.getCagnotte())
        {
            biblio.remove(test_appartient);
            cagnotte=cagnotte+l.getPrix();
            
            u.biblio.add(l);
            u.setCagnotte(u.getCagnotte()-l.getPrix());
        }
        else
        {
            System.out.println("\nVente impossible de '"+l.getTitre()+"' à "+u.getPrenom());
        }
    }
    
    public void echange(Media l1, Utilisateurs u, Media l2)
    {
        //Test si u1 as bien l1 et si u2 as bien l2
        int test_appartient=-1;
        for(int i=0;i<biblio.size();i++)
        {
            if(l1==biblio.get(i))
                test_appartient=i;
        }
        int test_appartient_u2=-1;
        for(int i=0;i<u.biblio.size();i++)
        {
            if(l2==u.biblio.get(i))
                test_appartient_u2=i;
        }

        //Calcul de la marge (en fonction du media le plus cher) ainsi que de la différence de prix
        double diffprix, marge;
        if(l1.getPrix()>l2.getPrix())
        {
            diffprix=l1.getPrix()-l2.getPrix();
            marge=l1.getPrix()*0.1;
        }else{
            diffprix=l2.getPrix()-l1.getPrix();
            marge=l2.getPrix()*0.1;
        }

        //échange
        if(test_appartient!=-1 && test_appartient_u2!=-1 && marge>=diffprix)
        {
            biblio.remove(test_appartient);
            biblio.add(l2);
            u.biblio.remove(test_appartient_u2);
            u.biblio.add(l1);
        }else{
            System.out.println("\nEchange impossible de '"+l1.getTitre()+"' à "+u.getPrenom()+" contre '"+l2.getTitre()+"'");
        }
    }

    public double getCagnotte() {
        return cagnotte;
    }
    public void setCagnotte(double c) {
        this.cagnotte = c;
    }
    

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }


    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }


    public String getMdp() {
        return mdp;
    }
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
    
    public List<Media> getbiblio() {
        return biblio;
    }
    public void setbiblio(List<Media> biblio) {
        this.biblio = biblio;
    }
}
