package com.mycompany.mavenproject_elivre;


public class Media {
    private String titre;
    private String auteur;
    private double prix;
    
    public Media(String titre, String auteur, double prix) {
        this.setTitre(titre);
        this.setAuteur(auteur);
        this.setPrix(prix);
    }

    public String affiche() {
        return "\tTitre : " + titre
    + "\n\tAuteur : " + auteur + "\n\tPrix : " + prix+"€";
    }
    
    
    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public double getPrix() {
        return prix;
    }
    public void setPrix(double prix) {
        this.prix = prix;
    }

}
