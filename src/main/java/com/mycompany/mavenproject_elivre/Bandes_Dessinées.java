package com.mycompany.mavenproject_elivre;


public class Bandes_Dessinées extends Livres {
    private String couleur;

    public Bandes_Dessinées(String titre2, String auteur2, double prix2, int nb_pages2, String couleur) {
	super(titre2,auteur2,prix2,nb_pages2);
        this.setCouleur(couleur);
    }
    public String affichebd() {
        return "> Bande Dessinée\n" + this.affichel() + "\n\tCouleur / Noir et Blanc : "+ couleur;
    }
    

    public String getCouleur() {
        return couleur;
    }
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
}
